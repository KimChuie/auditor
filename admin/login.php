<?php
	include("actions.php");
	if(isset($_SESSION['user_id'])){
		header("location:home");
	}
?>

<!doctype html>
<html lang="en">

<!-- Mirrored from bootstrap.gallery/kingfisher/dark-sidebar/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 29 Sep 2018 04:18:56 GMT -->
<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<meta name="description" content="Kingfisher Admin Panel" />
		<meta name="keywords" content="Login, Unify Login, Admin, Admin Dashboard, Dashboard, Bootstrap4, Sass, CSS3, HTML5, Responsive Dashboard, Responsive Admin Template, Admin Template, Best Admin Template, Bootstrap Template, Themeforest" />
		<meta name="author" content="Bootstrap Gallery" />
		<link rel="shortcut icon" href="img/favicon.ico" />
		<title>Hotel Booking System</title>

		<!--
			**********************
			**********************
			Common CSS files
			**********************
			**********************
		-->
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="css/bootstrap.min.css" />

		<!-- Icomoon Icons CSS -->
		<link rel="stylesheet" href="fonts/icomoon/icomoon.css" />

		<!-- Master CSS -->
		<link rel="stylesheet" href="css/main.css" />

	</head>

	<body style="background-image: url(img/bg.jpg);background-repeat:no-repeat;background-size: cover;">

		<!-- Container start -->
		<div class="container">

			<form method="post" action="actions">
				<div class="row justify-content-md-center">
					<div class="col-xl-4 col-lg-5 col-md-6 col-sm-12">
						<div class="login-screen">
							<div class="login-box">
								<a href="#" class="login-logo">
									<center>
									<h2 style="color: gray">LOGIN</h2>
									</center>
								</a>
								<div class="form-group">
									<input name="uname" type="text" class="form-control" placeholder="Username" />
								</div>
								<div class="form-group">
									<input name="pword" type="password" class="form-control" placeholder="Password" />
								</div>
								<div class="actions">
									<?php
										if(isset($_SESSION['err'])){
											?>
												<span class="badge badge-bdr badge-danger"><?php echo $_SESSION['err'];?></span><br>
											<?php
											session_unset($_SESSION['err']);
										}
									?>
									<button name="loginbtn" type="submit" class="btn btn-primary btn-block">Login</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</form>

		</div>
		<!-- Container end -->

	</body>

<!-- Mirrored from bootstrap.gallery/kingfisher/dark-sidebar/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 29 Sep 2018 04:18:56 GMT -->
</html>
