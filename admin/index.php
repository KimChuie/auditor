<?php
	session_start();
	include("actions.php");
?>

<!doctype html>
<html lang="en">
	
<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<meta name="description" content="Kingfisher Admin Panel" />
		<meta name="keywords" content="Admin, Dashboard, Bootstrap 4 Admin Dashboard, Bootstrap 4 Admin Template, Bootstrap 4 Admin Template, Sales, Admin Dashboard, Traffic, Tasks, Revenue, Orders, Invoices, Projects, Invoices, Dashboard, Bootstrap4, Sass, CSS3, HTML5, Responsive Dashboard, Responsive Admin Template, Admin Template, Best Admin Template, Bootstrap Template, Themeforest" />
		<meta name="author" content="Bootstrap Gallery" />
		<link rel="shortcut icon" href="img/favicon.ico" />
		<title>Kingfisher Bootstrap 4 Admin Dashboard</title>
		
		<!--
			**********************
			**********************
			Common CSS files
			**********************
			**********************
		-->
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="css/bootstrap.min.css" />

		<!-- Icomoon Icons CSS -->
		<link rel="stylesheet" href="fonts/icomoon/icomoon.css" />

		<!-- Master CSS -->
		<link rel="stylesheet" href="css/main.css" />

		<!-- Daterange CSS -->
		<link rel="stylesheet" href="vendor/daterange/daterange.css" />


		<!--
			**********************
			**********************
			Optional CSS files
			**********************
			**********************
		-->

		<!-- Datepickers CSS -->
		<link rel="stylesheet" href="css/datepicker.css" />

		<!-- jQueryUI CSS -->
		<link rel="stylesheet" href="css/jquery-ui.css" />

		<!-- Morris CSS -->
		<link rel="stylesheet" href="vendor/morris/morris.css" />

		<!-- Circliful CSS -->
		<link rel="stylesheet" href="vendor/circliful/circliful.css" />

		<!-- Tags CSS -->
		<link href="vendor/tags/tagmanager.css" rel="stylesheet" />

	</head>
	<body>

		<!-- BEGIN .app-wrap -->
		<div class="app-wrap">

			<!-- BEGIN .app-heading -->
			<header class="app-header">
				<div class="container-fluid">

					<!-- Row start -->
					<div class="row gutters">
						<div class="col-xl-7 col-lg-7 col-md-6 col-sm-7 col-7">
							
							<!-- BEGIN .logo -->
							<div class="logo-block">
								<a href="index-2.html" class="logo">
									<img src="img/logo.png" alt="Kingfisher Admin Dashboard" />
								</a>
								<a class="mini-nav-btn" href="#" id="onoffcanvas-nav">
									<i class="open"></i>
									<i class="open"></i>
									<i class="open"></i>
								</a>
								<a href="#app-side" data-toggle="onoffcanvas" class="onoffcanvas-toggler" aria-expanded="true">
									<i class="open"></i>
									<i class="open"></i>
									<i class="open"></i>
								</a>
							</div>
							<!-- END .logo -->

						</div>
						<div class="col-xl-5 col-lg-5 col-md-6 col-sm-5 col-5">

							<!-- Header actions start -->
							<ul class="header-actions">
								<li class="dropdown">
									<a href="#" id="notifications" data-toggle="dropdown" aria-haspopup="true">
										<i class="icon-notifications_none"></i>
										<span class="count-label">7</span>
									</a>
									<div class="dropdown-menu dropdown-menu-right lg" aria-labelledby="notifications">
										<ul class="imp-notify">
											<li>
												<div class="icon">W</div>
												<div class="details">
													<h6 class="username">Wilson</h6>
													<p class="desc">The best Dashboard design I have seen ever.</p>
													<p class="time">6:30 PM</p>
												</div>
											</li>
											<li>
												<div class="icon green">J</div>
												<div class="details">
													<h6 class="username">John Smith</h6>
													<p class="desc">Jhonny sent you a message. Read now!</p>
													<p class="time">7:20 PM</p>
												</div>
											</li>
											<li>
												<div class="icon red">R</div>
												<div class="details">
													<h6 class="username">Zustin Mezzell</h6>
													<p class="desc">Stella, Added you as a Friend. Accept it!</p>
													<p class="time">3:45 PM</p>
												</div>
											</li>
										</ul>
									</div>
								</li>
								<li>
									<a href="#" id="todos" data-toggle="dropdown" aria-haspopup="true">
										<i class="icon-person_outline"></i>
										<span class="count-label red">5</span>
									</a>
									<div class="dropdown-menu dropdown-menu-right lg" aria-labelledby="todos">
										<ul class="tasks-widget">
											<li>
												<p>Task #48<span>90%</span></p>
												<div class="progress">
													<div class="progress-bar bg-success" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100" style="width: 90%">
														<span class="sr-only">90% Complete (success)</span>
													</div>
												</div>
											</li>
											<li>
												<p>Task #98<span>60%</span></p>
												<div class="progress">
													<div class="progress-bar bg-info" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
														<span class="sr-only">60% Complete (success)</span>
													</div>
												</div>
											</li>
											<li>
												<p>Task #7<span>40%</span></p>
												<div class="progress">
													<div class="progress-bar bg-danger" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
														<span class="sr-only">40% Complete (success)</span>
													</div>
												</div>
											</li>
										</ul>
									</div>
								</li>
								<li class="dropdown">
									<a href="#" id="userSettings" class="user-settings" data-toggle="dropdown" aria-haspopup="true">
										<span class="avatar">ER<span class="status online"></span></span>
										<span class="user-name">Emily Russell</span>
										<i class="icon-chevron-small-down downarrow"></i>
									</a>
									<div class="dropdown-menu lg dropdown-menu-right" aria-labelledby="userSettings">
										<div class="admin-settings">
											<ul class="admin-settings-list">
												<li>
													<a href="profile.html">
														<span class="icon icon-face"></span>
														<span class="text-name">My Profile</span>
														<span class="badge badge-success">75% Complete</span>
													</a>
												</li>
												<li>
													<a href="#">
														<span class="icon icon-notifications_none"></span>
														<span class="text-name">Notifications</span>
														<span class="badge badge-orange">12</span>
													</a>
												</li>
												<li>
													<a href="#">
														<span class="icon icon-av_timer"></span>
														<span class="text-name">Secure Account</span>
													</a>
												</li>
											</ul>
											<div class="actions">
												<a href="login.html" class="btn btn-primary">Logout</a>
											</div>
										</div>
									</div>
								</li>
							</ul>
							<!-- Header actions end -->

						</div>
					</div>
					<!-- Row start -->

				</div>
			</header>
			<!-- END: .app-heading -->

			<!-- BEGIN .app-container -->
			<div class="app-container">

				<!-- BEGIN .app-side -->
				<aside class="app-side fixed" id="app-side">

					<!-- BEGIN .side-content -->
					<div class="side-content ">
						<!-- Nav scroll start -->
						<div class="sidebarNavScroll">

							<!-- BEGIN .side-nav -->
							<nav class="side-nav">
								
								<!-- BEGIN: side-nav-content -->
								<ul class="unifyMenu" id="unifyMenu">
									<li class="selected">
										<a href="index-2.html">
											<span class="has-icon">
												<i class="icon-laptop_windows"></i>
											</span>
											<span class="nav-title">Dashboard</span>
										</a>
									</li>
									<li>
										<a href="tasks.html">
											<span class="has-icon">
												<i class="icon-assignment"></i>
											</span>
											<span class="nav-title">Tasks</span>
											<span class="badge">7</span>
										</a>
									</li>
									<li>
										<a href="chat.html">
											<span class="has-icon">
												<i class="icon-message2"></i>
											</span>
											<span class="nav-title">Chat</span>
										</a>
									</li>
									<li>
										<a href="index2.html">
											<span class="has-icon">
												<i class="icon-slow_motion_video"></i>
											</span>
											<span class="nav-title">Dashboard 2</span>
										</a>
									</li>
									<li>
										<a href="widgets.html">
											<span class="has-icon">
												<i class="icon-style"></i>
											</span>
											<span class="nav-title">Widgets</span>
											<span class="badge green">20+</span>
										</a>
									</li>
									<li>
										<a href="#" class="has-arrow" aria-expanded="false">
											<span class="has-icon">
												<i class="icon-center_focus_strong"></i>
											</span>
											<span class="nav-title">Forms</span>
											<span class="lbl"></span>
										</a>
										<ul aria-expanded="false">
											<li>
												<a href='form-inputs.html'>Form Inputs</a>
											</li>
											<li>
												<a href='input-groups.html'>Inputs Groups</a>
											</li>
											<li>
												<a href='bs-select.html'>BS Select</a>
											</li>
											<li>
												<a href='range-sliders.html'>Range Sliders</a>
											</li>
											<li>
												<a href='summernote-editor.html'>Editor</a>
											</li>
											<li>
												<a href='checkbox-radio.html'>Checkbox &amp; Radio</a>
											</li>
											<li>
												<a href='contact.html'>Contact Form</a>
											</li>
											<li>
												<a href='contact2.html'>Contact Form #2</a>
											</li>
											<li>
												<a href='contact3.html'>Contact Form #3</a>
											</li>
											<li>
												<a href='contact4.html'>Contact Form #4</a>
											</li>
											<li>
												<a href='subscribe-form.html'>Subscribe Form</a>
											</li>
											<li>
												<a href='checkout-form.html'>Checkout Form</a>
											</li>
										</ul>
									</li>
									<li>
										<a href="custom-tables.html">
											<span class="has-icon">
												<i class="icon-border_clear"></i>
											</span>
											<span class="nav-title">Custom Tables</span>
										</a>
									</li>
									<li>
										<a href="#" class="has-arrow" aria-expanded="false">
											<span class="has-icon">
												<i class="icon-layers"></i>
											</span>
											<span class="nav-title">Layouts</span>
											<span class="lbl red"></span>
										</a>
										<ul aria-expanded="false">
											<li>
												<a href='custom-drag.html'>Custom Panels</a>
											</li>
											<li>
												<a href='layout.html'>Default Layout</a>
											</li>
											<li>
												<a href='fixed-sidebar.html'>Fixed Sidebar</a>
											</li>
											<li>
												<a href='boxed.html'>Boxed Layout</a>
											</li>
											<li>
												<a href='fullscreen.html'>Full Screen</a>
											</li>
											<li>
												<a href='layout-no-quick-actions.html'>No Quick Links</a>
											</li>
										</ul>
									</li>
									<li>
										<a href="#" class="has-arrow" aria-expanded="false">
											<span class="has-icon">
												<i class="icon-tabs-outline"></i>
											</span>
											<span class="nav-title">Pages</span>
											<span class="lbl green"></span>
										</a>
										<ul aria-expanded="false">
											<li>
												<a href='profile.html'>User Profile</a>
											</li>
											<li>
												<a href='contacts.html'>Contacts</a>
											</li>
											<li>
												<a href='calendar.html'>Calendar</a>
											</li>
											<li>
												<a href='invoice.html'>Invoice</a>
											</li>
											<li>
												<a href='timeline.html'>Timeline</a>
											</li>
											<li>
												<a href="pricing.html">Pricing</a>
											</li>
											<li>
												<a href="faq.html">Faq's</a>
											</li>
											<li>
												<a href="search.html">Search results</a>
											</li>
										</ul>
									</li>
									<li>
										<a href="cards.html">
											<span class="has-icon">
												<i class="icon-book3"></i>
											</span>
											<span class="nav-title">Cards</span>
										</a>
									</li>
									<li>
										<a href="#" class="has-arrow" aria-expanded="false">
											<span class="has-icon">
												<i class="icon-public"></i>
											</span>
											<span class="nav-title">Maps</span>
											<span class="lbl yellow"></span>
										</a>
										<ul aria-expanded="false">
											<li>
												<a href='map-skins.html'>Snazzy Maps</a>
											</li>
											<li>
												<a href='google-maps.html'>Google Maps</a>
											</li>
											<li>
												<a href='vector-maps.html'>Vector Maps</a>
											</li>
										</ul>
									</li>
									<li>
										<a href="gallery.html">
											<span class="has-icon">
												<i class="icon-image2"></i>
											</span>
											<span class="nav-title">Gallery</span>
										</a>
									</li>
									<li>
										<a href="comments.html">
											<span class="has-icon">
												<i class="icon-chat_bubble_outline"></i>
											</span>
											<span class="nav-title">Comments</span>
										</a>
									</li>
									<li>
										<a href="datepickers.html">
											<span class="has-icon">
												<i class="icon-insert_invitation"></i>
											</span>
											<span class="nav-title">Datepickers</span>
										</a>
									</li>
									<li>
										<a href="#" class="has-arrow" aria-expanded="false">
											<span class="has-icon">
												<i class="icon-chart-area-outline"></i>
											</span>
											<span class="nav-title">Graphs</span>
											<span class="lbl"></span>
										</a>
										<ul aria-expanded="false">
											<li>
												<a href='c3-graphs.html'>C3 Graphs</a>
											</li>
											<li>
												<a href='flot.html'>Flot Graphs</a>
											</li>
											<li>
												<a href='morris.html'>Morris Graphs</a>
											</li>
										</ul>
									</li>
									<li>
										<a href="#" class="has-arrow" aria-expanded="false">
											<span class="has-icon">
												<i class="icon-beaker"></i>
											</span>
											<span class="nav-title">UI Elements</span>
											<span class="lbl yellow"></span>
										</a>
										<ul aria-expanded="false">
											<li>
												<a href='general-elements.html'>General Elements</a>
											</li>
											<li>
												<a href='buttons.html'>Buttons</a>
											</li>
											<li>
												<a href='tabs.html'>Tabs</a>
											</li>
											<li>
												<a href="modals.html">Modals</a>
											</li>
											<li>
												<a href='accordion.html'>Accordion</a>
											</li>
											<li>
												<a href="labels-badges.html">Labels &amp; Badges</a>
											</li>
											<li>
												<a href='notifications.html'>Notifications</a>
											</li>
											<li>
												<a href='carousel.html'>Carousels</a>
											</li>
											<li>
												<a href='list-items.html'>List Items</a>
											</li>
											<li>
												<a href='cards.html'>Cards</a>
											</li>
											<li>
												<a href='navbars.html'>Navbars</a>
											</li>
											<li>
												<a href='popovers-tooltips.html'>Popovers &amp; Tooltips</a>
											</li>
											<li>
												<a href='progress.html'>Progress Bars</a>
											</li>
											<li>
												<a href='pagination.html'>Pagination</a>
											</li>
											<li>
												<a href='typography.html'>Typography</a>
											</li>
											<li>
												<a href='media-objects.html'>Media Objects</a>
											</li>
											<li>
												<a href='icons.html'>Icons</a>
											</li>
										</ul>
									</li>
									<li>
										<a href="tables.html">
											<span class="has-icon">
												<i class="icon-border_outer"></i>
											</span>
											<span class="nav-title">Tables</span>
										</a>
									</li>
									<li>
										<a href="datatables.html">
											<span class="has-icon">
												<i class="icon-border_all"></i>
											</span>
											<span class="nav-title">Data Tables</span>
										</a>
									</li>
									<li>
										<a href="#" class="has-arrow" aria-expanded="false">
											<span class="has-icon">
												<i class="icon-lock_outline"></i>
											</span>
											<span class="nav-title">Authentication</span>
											<span class="lbl red"></span>
										</a>
										<ul aria-expanded="false">
											<li>
												<a href='login.html'>Login</a>
											</li>
											<li>
												<a href='signup.html'>Signup</a>
											</li>
											<li>
												<a href='forgot-pwd.html'>Forgot Password</a>
											</li>
											<li>
												<a href="locked-screen.html">Locked Screen</a>
											</li>
											<li>
												<a href='error404.html'>Error 404</a>
											</li>
											<li>
												<a href='error505.html'>Error 505</a>
											</li>
											<li>
												<a href='secure.html'>Secure Account</a>
											</li>
										</ul>
									</li>
									<li>
										<a href="#" class="has-arrow" aria-expanded="false">
											<span class="has-icon">
												<i class="icon-format_align_left"></i>
											</span>
											<span class="nav-title">Multi Level Nav</span>
											<span class="lbl"></span>
										</a>
										<ul aria-expanded="false">
											<li>
												<a href='#'>Level #One</a>
											</li>
											<li>
												<a href="#" class="has-arrow" aria-expanded="false">
													<span class="nav-title">Level #One</span>
												</a>
												<ul aria-expanded="false">
													<li>
														<a href='#'>Level #Two</a>
													</li>
													<li>
														<a href='#'>Level #Two</a>
													</li>
													<li>
														<a href='#'>Level #Two</a>
													</li>
												</ul>
											</li>
										</ul>
									</li>
								</ul>
								<!-- END: side-nav-content -->

							</nav>
							<!-- END: .side-nav -->

							<!-- Sidebar widgets start -->
							<div class="sidebar-widget">
								<ul class="contributions">
									<li>
										<p>Nice Template <span>$9180</span></p>
										<div class="progress sm mb-1">
											<div class="progress-bar bg-orange" role="progressbar" style="width: 75%;" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
										</div>
									</li>
									<li>
										<p>New Project <span>$5179</span></p>
										<div class="progress sm mb-1">
											<div class="progress-bar bg-success" role="progressbar" style="width: 50%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
										</div>
									</li>
									<li>
										<p>Balance <span>$12595</span></p>
										<div class="progress sm mb-1">
											<div class="progress-bar bg-info" role="progressbar" style="width: 35%;" aria-valuenow="35" aria-valuemin="0" aria-valuemax="100"></div>
										</div>
									</li>
								</ul>
							</div>
							<!-- Sidebar widgets end -->

						</div>
						<!-- Nav scroll end -->

					</div>
					<!-- END: .side-content -->

				</aside>
				<!-- END: .app-side -->

				<!-- BEGIN .app-main -->
				<div class="app-main">

					<!-- BEGIN .main-heading -->
					<header class="main-heading">
						<div class="container-fluid">
							<div class="row">
								<div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
									<div class="page-icon">
										<i class="icon-laptop_windows"></i>
									</div>
									<div class="page-title">
										<h3>Dashboard</h3>
									</div>
								</div>
							</div>
						</div>
					</header>
					<!-- END: .main-heading -->

					<!-- BEGIN .main-content -->
					<div class="main-content">

						<!-- Row start -->
						<div class="row gutters">
							<div class="col-xl-3 col-lg-3 col-md-3 col-sm-6">
								<div class="simple-widget">

									<!-- FETCH TOTAL ROOMS -->
									<?php
										$db = db();
										$res = $db->query("select * from rooms");
										$t_room = 0;
										while($row=$res->fetch_array()){
											$t_room++;
										}
									?>
									<!-- FETCH TOTAL ROOMS END -->

									<h3><?php echo $t_room;?></h3>
									<p>Total Rooms</p>
									<div class="progress sm">
										<div class="progress-bar" role="progressbar" style="width: 100%;" aria-valuenow="37" aria-valuemin="0" aria-valuemax="100"></div>
									</div>
								</div>
							</div>
							<div class="col-xl-3 col-lg-3 col-md-3 col-sm-6">
								<div class="simple-widget">

									<!-- FETCH VACANT ROOMS -->
									<?php
										$db = db();
										$res = $db->query("select * from rooms where status = 3");
										$v_num = 0;
										while($row=$res->fetch_array()){
											$v_num++;
										}
									?>
									<!-- FETCH VACANT ROOMS END -->

									<h3><?php echo $v_num;?></h3>
									<p>Vacant Rooms</p>
									<div class="progress sm">
										<div class="progress-bar" role="progressbar" style="width: 48%;" aria-valuenow="48" aria-valuemin="0" aria-valuemax="100"></div>
									</div>
								</div>
							</div>
							<div class="col-xl-3 col-lg-3 col-md-3 col-sm-6">
								<div class="simple-widget">

									<!-- FETCH RESERVED ROOMS -->
									<?php
										$db = db();
										$res = $db->query("select * from rooms where status = 2");
										$r_num = 0;
										while($row=$res->fetch_array()){
											$r_num++;
										}
									?>
									<!-- FETCH RESERVED ROOMS END -->
									<h3><?php echo $r_num;?></h3>
									<p>Reserved Rooms</p>
									<div class="progress sm">
										<div class="progress-bar" role="progressbar" style="width: 65%;" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100"></div>
									</div>
								</div>
							</div>
							<div class="col-xl-3 col-lg-3 col-md-3 col-sm-6">
								<div class="simple-widget orange">

									<!-- FETCH OCCUPIED ROOMS -->
									<?php
										$db = db();
										$res = $db->query("select * from rooms where status = 1");
										$o_num = 0;
										while($row=$res->fetch_array()){
											$o_num++;
										}
									?>
									<!-- FETCH OCCUPIED ROOMS END -->

									<h3><?php echo $o_num;?></h3>
									<p>Occupied Rooms</p>
									<div class="progress sm">
										<div class="progress-bar" role="progressbar" style="width: 80%;" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
									</div>
								</div>
							</div>
						</div>
						<!-- Row end -->

						<!-- Row start -->
						<div class="row gutters">
							<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
								<div class="card">
									<div class="card-header">Rooms</div>
									<!--*************************
										*************************
										*************************
										Basic Table start
										*************************
										*************************
										*************************-->
										<div class="table-responsive">
											<table class="table m-0">
												<thead class="thead-dark">
													<tr>
														<center>
														<th>Room #</th>
														<th>Status</th>
														<th style="text-align: center;">Actions</th>

													</tr>
												</thead>
												<tbody>
													<!-- FETCH ROOMS -->
														<?php
															$db = db();
															$res = $db->query("select * from rooms");
															while($row=$res->fetch_array()){
																echo "<tr>";

																echo "<td>".$row['room_id']."</td>";

																$res2 = $db->query("select * from status where st_id = ".$row['status']);
																while($row2=$res2->fetch_array()){
																	echo "<td>".$row2['status']."</td>";
																}
																if($row['status']==3){
																	?>
																		<td>
																			<center>
																			<button type="button" class="btn btn-success">Reserve</button> ||
																			<button type="button" class="btn btn-light" data-toggle="modal" data-target="#exampleModal<?php echo $row['room_id'];?>">Check In</button>
																			</center>
																		</td>
																	<?php
																}else if($row['status']==2){
																	?>
																		<td>
																			<center>
																			<button type="button" class="btn btn-dark">View Room Info</button> ||
																			<button type="button" class="btn btn-light">Check In</button> ||
																			<button type="button" class="btn btn-danger">Free Up</button>
																			</center>
																		</td>
																	<?php
																}else if($row['status']==1){
																	?>
																		<td>
																			<center>
																				<button type="button" class="btn btn-dark">View Room Info</button> ||
																				<button type="button" class="btn btn-danger">Free Ups</button>
																			</center>
																		</td>
																	<?php
																}
																echo "</tr>";

																?>
																	<!--*************************
																	*************************
																	*************************
																	 Modal live example start 
																	*************************
																	*************************
																	*************************-->

																	<!-- Modal -->
																	<form method="post" action="actions.php">
																	<div class="modal fade" id="exampleModal<?php echo $row['room_id']?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
																		<div class="modal-dialog" role="document">
																			<div class="modal-content">
																				<div class="modal-header">
																					<h5 class="modal-title" id="exampleModalLabel">Fill the Fields</h5>
																					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																						<span aria-hidden="true">&times;</span>
																					</button>
																				</div>
																				<div class="modal-body">
																						<div class="input-group input-group-sm mb-3">
																						  <div class="input-group-prepend">
																						    <span class="input-group-text" id="inputGroup-sizing-sm">Room Number</span>
																						  </div>
																						  <input name="cid" value="<?php echo $row['room_id'];?>" name="id" type="text" class="form-control" aria-label="Small" aria-describedby="inputGroup-sizing-sm" readonly>
																						</div>
																						<div class="input-group input-group-sm mb-3">
																						  <div class="input-group-prepend">
																						    <span class="input-group-text" id="inputGroup-sizing-sm">First Name</span>
																						  </div>
																						  <input name="fname" type="text" class="form-control" aria-label="Small" aria-describedby="inputGroup-sizing-sm" required>
																						</div>
																						<div class="input-group input-group-sm mb-3">
																						  <div class="input-group-prepend">
																						    <span class="input-group-text" id="inputGroup-sizing-sm">Last Name</span>
																						  </div>
																						  <input name="lname" type="text" class="form-control" aria-label="Small" aria-describedby="inputGroup-sizing-sm" required>
																						</div>
																				</div>
																				<div class="modal-footer">
																					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
																					<button type="submit" class="btn btn-primary" name="check-in-btn">Check In</button>
																				</div>
																			</div>
																		</div>
																	</div>
																	</form>
																	<!--*************************
																	*************************
																	*************************
																	 Modal live example end 
																	*************************
																	*************************
																	*************************-->
																<?php

															}
														?>
														<!-- FETCH ROOMS END -->
												</tbody>
											</table>
										</div>
										<!--*************************
										*************************
										*************************
										Basic Table end
										*************************
										*************************
										*************************-->

										


									</div>
							</div>
						</div>
						<!-- Row end -->
							</div>
						</div>
					<!-- END: .main-content -->

					<!-- BEGIN .main-footer -->
					<footer class="main-footer">

					</footer>
					<!-- END: .main-footer -->

				</div>
				<!-- END: .app-main -->

			</div>
			<!-- END: .app-container -->

		</div>
		<!-- END: .app-wrap -->


		<!--
			**********************
			**********************
			Common JS files
			**********************
			**********************
		-->

		<!-- jQuery JS. -->
		<script src="js/jquery.js"></script>

		<!-- Info: jQuery UI is only required for datepicker or any jQueryUI related plugins -->
		<script src="js/jquery-ui.min.js"></script>

		<!-- Tether Js, then other JS. -->
		<script src="js/popper.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="vendor/unifyMenu/unifyMenu.js"></script>
		<script src="vendor/onoffcanvas/onoffcanvas.js"></script>
		<script src="js/moment.js"></script>

		<!-- News Ticker JS -->
		<script src="vendor/newsticker/newsTicker.min.js"></script>
		<script src="vendor/newsticker/custom-newsTicker.js"></script>

		<!-- Slimscroll JS -->
		<script src="vendor/slimscroll/slimscroll.min.js"></script>
		<script src="vendor/slimscroll/custom-scrollbar.js"></script>

		<!-- Daterange JS -->
		<script src="vendor/daterange/daterange.js"></script>
		<script src="vendor/daterange/custom-daterange.js"></script>


		<!--
			**********************
			**********************
			Optional JS files - Plugns
			**********************
			**********************
		-->

		<!-- Morris Graphs JS -->
		<script src="vendor/morris/raphael-min.js"></script>
		<script src="vendor/morris/morris.min.js"></script>
		<script src="vendor/morris/custom/areaChart.js"></script>
		<script src="vendor/morris/custom/line-chart.js"></script>
		<script src="vendor/morris/custom/multibar.js"></script>

		<!-- Circliful JS -->
		<script src="vendor/circliful/circliful.min.js"></script>
		<script src="vendor/circliful/circliful.custom.js"></script>

		<!-- Peity JS -->
		<script src="vendor/peity/peity.min.js"></script>
		<script src="vendor/peity/custom-peity.js"></script>

		<!-- Tag Manager JS -->
		<script src="vendor/tags/tagmanager.js"></script>
		<script src="vendor/tags/tagmanager-custom.js"></script>

		<!-- Common JS -->
		<script src="js/common.js"></script>

		<script>
			//Datepicker
			$(function() {
				$("#datepicker").datepicker();
			});
		</script>
		
	</body>
</html>