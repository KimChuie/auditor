<?php
	include("actions.php");
	if(!isset($_SESSION['user_id'])){
		header("location:login");
	}
?>

<!doctype html>
<html lang="en">
	
<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<meta name="description" content="Kingfisher Admin Panel" />
		<meta name="keywords" content="Admin, Dashboard, Bootstrap 4 Admin Dashboard, Bootstrap 4 Admin Template, Bootstrap 4 Admin Template, Sales, Admin Dashboard, Traffic, Tasks, Revenue, Orders, Invoices, Projects, Invoices, Dashboard, Bootstrap4, Sass, CSS3, HTML5, Responsive Dashboard, Responsive Admin Template, Admin Template, Best Admin Template, Bootstrap Template, Themeforest" />
		<meta name="author" content="Bootstrap Gallery" />
		<link rel="shortcut icon" href="img/favicon.ico" />
		<title>Kingfisher Bootstrap 4 Admin Dashboard</title>
		
		<!--
			**********************
			**********************
			Common CSS files
			**********************
			**********************
		-->
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="css/bootstrap.min.css" />

		<!-- Icomoon Icons CSS -->
		<link rel="stylesheet" href="fonts/icomoon/icomoon.css" />

		<!-- Master CSS -->
		<link rel="stylesheet" href="css/main.css" />

		<!-- Daterange CSS -->
		<link rel="stylesheet" href="vendor/daterange/daterange.css" />


		<!--
			**********************
			**********************
			Optional CSS files
			**********************
			**********************
		-->

		<!-- Datepickers CSS -->
		<link rel="stylesheet" href="css/datepicker.css" />

		<!-- jQueryUI CSS -->
		<link rel="stylesheet" href="css/jquery-ui.css" />

		<!-- Morris CSS -->
		<link rel="stylesheet" href="vendor/morris/morris.css" />

		<!-- Circliful CSS -->
		<link rel="stylesheet" href="vendor/circliful/circliful.css" />

		<!-- Tags CSS -->
		<link href="vendor/tags/tagmanager.css" rel="stylesheet" />

	</head>
	<body>

		<!-- BEGIN .app-wrap -->
		<div class="app-wrap">

			<!-- BEGIN .app-heading -->
			<header class="app-header">
				<div class="container-fluid">

					<!-- Row start -->
					<div class="row gutters">
						<div class="col-xl-7 col-lg-7 col-md-6 col-sm-7 col-7">
							
							<!-- BEGIN .logo -->
							<div class="logo-block">
								<a href="index-2.html" class="logo">
									<img src="img/logo.png" alt="Kingfisher Admin Dashboard" />
								</a>
								<a href="#app-side" data-toggle="onoffcanvas" class="onoffcanvas-toggler" aria-expanded="true">
									<i class="open"></i>
									<i class="open"></i>
									<i class="open"></i>
								</a>
							</div>
							<!-- END .logo -->

						</div>
						<div class="col-xl-5 col-lg-5 col-md-6 col-sm-5 col-5">

							<!-- Header actions start -->
							<ul class="header-actions">

								<?php
									$db = db();
									$rs = $db->query("select * from tbl_user where user_id = ".$_SESSION['user_id']);
									while($row = $rs->fetch_array()){
										$_SESSION['name'] = $row['fname'];
									}
								?>

								<li class="dropdown">
									<a href="#" id="userSettings" class="user-settings" data-toggle="dropdown" aria-haspopup="true">
										<span class="avatar"><?php echo substr($_SESSION['name'], 0,1);?><span class="status online"></span></span>
										<span class="user-name"><?php echo $_SESSION['name'];?></span>
										<i class="icon-chevron-small-down downarrow"></i>
									</a>
									<div class="dropdown-menu lg dropdown-menu-right" aria-labelledby="userSettings">
										<div class="admin-settings">
											<ul class="admin-settings-list">
												<li>
													<a href="profile.html">
														<span class="icon icon-face"></span>
														<span class="text-name">Manage Users</span>
													</a>
												</li>
											</ul>
											<div class="actions">
												<a href="actions?logout" class="btn btn-primary">Logout</a>
											</div>
										</div>
									</div>
								</li>
							</ul>
							<!-- Header actions end -->

						</div>
					</div>
					<!-- Row start -->

				</div>
			</header>
			<!-- END: .app-heading -->

			<!-- BEGIN .app-container -->
			<div class="app-container">


					<!-- BEGIN .main-content -->
					<div class="main-content">

						<!-- Row end -->

						<!-- Row start -->
						<div class="row gutters">
							<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
								<div class="card">
									<div class="card-header">Users</div>
									<!--*************************
										*************************
										*************************
										Basic Table start
										*************************
										*************************
										*************************-->
										<div class="table-responsive">
											<table class="table m-0">
												<thead class="thead-dark">
													<tr>
														<center>
														<th>User ID</th>
														<th>Name</th>
														<th style="text-align: center;">Actions</th>

													</tr>
												</thead>
												<tbody>
													<!-- FETCH USER -->
														<?php
															$db = db();
															$res = $db->query("select * from tbl_user");
															while($row=$res->fetch_array()){
																echo "<tr>";

																echo "<td>".$row['user_id']."</td>";
																echo "<td>".$row['fname']." ".$row['lname']."</td>";
																?>
																<td>
																	<center>
																	<button type="button" class="btn btn-success" data-toggle="modal" data-target="#exampleModal<?php echo $row['user_id'];?>1010">Update</button>||
																	<button type="button" class="btn btn-light" data-toggle="modal" data-target="#exampleModal<?php echo $row['user_id'];?>">Remove</button>
																	</center>
																</td>
																<?php

																?>
																	<!--*************************
																	*************************
																	*************************
																	 Modal live example start 
																	*************************
																	*************************
																	*************************-->

																	<!-- Modal -->
																	<form method="post" action="actions.php">
																	<div class="modal fade" id="exampleModal<?php echo $row['user_id']?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
																		<div class="modal-dialog" role="document">
																			<div class="modal-content">
																				<div class="modal-header">
																					<h5 class="modal-title" id="exampleModalLabel">Fill the Fields</h5>
																					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																						<span aria-hidden="true">&times;</span>
																					</button>
																				</div>
																				<div class="modal-body">
																						<div class="input-group input-group-sm mb-3">
																						  <div class="input-group-prepend">
																						    <span class="input-group-text" id="inputGroup-sizing-sm">User ID</span>
																						  </div>
																						  <input name="cid" value="<?php echo $row['user_id'];?>" name="id" type="text" class="form-control" aria-label="Small" aria-describedby="inputGroup-sizing-sm" readonly>
																						</div>
																						<div class="input-group input-group-sm mb-3">
																						  <div class="input-group-prepend">
																						    <span class="input-group-text" id="inputGroup-sizing-sm">First Name</span>
																						  </div>
																						  <input name="fname" type="text" class="form-control" aria-label="Small" aria-describedby="inputGroup-sizing-sm" required>
																						</div>
																						<div class="input-group input-group-sm mb-3">
																						  <div class="input-group-prepend">
																						    <span class="input-group-text" id="inputGroup-sizing-sm">Last Name</span>
																						  </div>
																						  <input name="lname" type="text" class="form-control" aria-label="Small" aria-describedby="inputGroup-sizing-sm" required>
																						</div>
																				</div>
																				<div class="modal-footer">
																					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
																					<button type="submit" class="btn btn-primary" name="check-in-btn">Check In</button>
																				</div>
																			</div>
																		</div>
																	</div>
																	</form>
																	<!--*************************
																	*************************
																	*************************
																	 Modal live example end 
																	*************************
																	*************************
																	*************************-->

																	<!--*************************
																	*************************
																	*************************
																	 Modal live example start 
																	*************************
																	*************************
																	*************************-->

																	<!-- Modal -->
																	<form method="post" action="actions.php">
																	<div class="modal fade" id="exampleModal<?php echo $row['user_id']?>1010" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
																		<div class="modal-dialog" role="document">
																			<div class="modal-content">
																				<div class="modal-header">
																					<h5 class="modal-title" id="exampleModalLabel">Fill the Fields</h5>
																					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																						<span aria-hidden="true">&times;</span>
																					</button>
																				</div>
																				<div class="modal-body">
																						<div class="input-group input-group-sm mb-3">
																						  <div class="input-group-prepend">
																						    <span class="input-group-text" id="inputGroup-sizing-sm">User ID</span>
																						  </div>
																						  <input name="cid" value="<?php echo $row['user_id'];?>" name="id" type="text" class="form-control" aria-label="Small" aria-describedby="inputGroup-sizing-sm" readonly>
																						</div>
																						<div class="input-group input-group-sm mb-3">
																						  <div class="input-group-prepend">
																						    <span class="input-group-text" id="inputGroup-sizing-sm">First Name</span>
																						  </div>
																						  <input name="fname" type="text" class="form-control" aria-label="Small" aria-describedby="inputGroup-sizing-sm" required>
																						</div>
																						<div class="input-group input-group-sm mb-3">
																						  <div class="input-group-prepend">
																						    <span class="input-group-text" id="inputGroup-sizing-sm">Last Name</span>
																						  </div>
																						  <input name="lname" type="text" class="form-control" aria-label="Small" aria-describedby="inputGroup-sizing-sm" required>
																						</div>
																				</div>
																				<div class="modal-footer">
																					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
																					<button type="submit" class="btn btn-primary" name="reserve-btn">Reserve</button>
																				</div>
																			</div>
																		</div>
																	</div>
																	</form>
																	<!--*************************
																	*************************
																	*************************
																	 Modal live example end 
																	*************************
																	*************************
																	*************************-->

																	<!--*************************
																	*************************
																	*************************
																	 Modal live example start 
																	*************************
																	*************************
																	*************************-->

																	<!-- Modal -->
																	<form method="post" action="actions.php">
																	<div class="modal fade" id="exampleModal<?php echo $row['user_id']?>101010" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
																		<div class="modal-dialog" role="document">
																			<div class="modal-content">
																				<div class="modal-header">
																					<h5 class="modal-title" id="exampleModalLabel">User ID: <?php echo $row['user_id']?></h5>
																					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																						<span aria-hidden="true">&times;</span>
																					</button>
																				</div>
																				<div class="modal-body">
																						<div class="input-group input-group-sm mb-3">
																						  <div class="input-group-prepend">
																						    <span class="input-group-text" id="inputGroup-sizing-sm">User ID</span>
																						  </div>
																						  <input name="cid" value="<?php echo $row['user_id'];?>" name="id" type="text" class="form-control" aria-label="Small" aria-describedby="inputGroup-sizing-sm" readonly>
																						</div>
																						<div class="input-group input-group-sm mb-3">
																						  <div class="input-group-prepend">
																						    <span class="input-group-text" id="inputGroup-sizing-sm">First Name</span>
																						  </div>
																						  <input value="<?php echo $row['fname']?>" type="text" class="form-control" aria-label="Small" aria-describedby="inputGroup-sizing-sm" readonly>
																						</div>
																						<div class="input-group input-group-sm mb-3">
																						  <div class="input-group-prepend">
																						    <span class="input-group-text" id="inputGroup-sizing-sm">Last Name</span>
																						  </div>
																						  <input value="<?php echo $row['lname']?>" type="text" class="form-control" aria-label="Small" aria-describedby="inputGroup-sizing-sm" readonly>
																						</div>
																				</div>
																				<div class="modal-footer">
																					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
																				</div>
																			</div>
																		</div>
																	</div>
																	</form>
																	<!--*************************
																	*************************
																	*************************
																	 Modal live example end 
																	*************************
																	*************************
																	*************************-->

																	<!--*************************
																	*************************
																	*************************
																	 Modal live example start 
																	*************************
																	*************************
																	*************************-->

																	<!-- Modal -->
																	<form method="post" action="actions.php">
																	<div class="modal fade" id="exampleModal<?php echo $row['user_id']?>10101010" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
																		<div class="modal-dialog" role="document">
																			<div class="modal-content">
																				<div class="modal-header">
																					<h5 class="modal-title" id="exampleModalLabel">Checked in at Room <?php echo $row['user_id']?></h5>
																					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																						<span aria-hidden="true">&times;</span>
																					</button>
																				</div>
																				<div class="modal-body">
																						<div class="input-group input-group-sm mb-3">
																						  <div class="input-group-prepend">
																						    <span class="input-group-text" id="inputGroup-sizing-sm">Room Number</span>
																						  </div>
																						  <input name="cid" value="<?php echo $row['user_id'];?>" name="id" type="text" class="form-control" aria-label="Small" aria-describedby="inputGroup-sizing-sm" readonly>
																						</div>
																						<div class="input-group input-group-sm mb-3">
																						  <div class="input-group-prepend">
																						    <span class="input-group-text" id="inputGroup-sizing-sm">First Name</span>
																						  </div>
																						  <input value="<?php echo $row['fname']?>" type="text" class="form-control" aria-label="Small" aria-describedby="inputGroup-sizing-sm" readonly>
																						</div>
																						<div class="input-group input-group-sm mb-3">
																						  <div class="input-group-prepend">
																						    <span class="input-group-text" id="inputGroup-sizing-sm">Last Name</span>
																						  </div>
																						  <input value="<?php echo $row['lname']?>" type="text" class="form-control" aria-label="Small" aria-describedby="inputGroup-sizing-sm" readonly>
																						</div>
																				</div>
																				<div class="modal-footer">
																					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
																				</div>
																			</div>
																		</div>
																	</div>
																	</form>
																	<!--*************************
																	*************************
																	*************************
																	 Modal live example end 
																	*************************
																	*************************
																	*************************-->
																<?php

															}
														?>
														<!-- FETCH ROOMS END -->
												</tbody>
											</table>
										</div>
										<!--*************************
										*************************
										*************************
										Basic Table end
										*************************
										*************************
										*************************-->

										


									</div>
							</div>
						</div>
						<!-- Row end -->
							</div>
						</div>
					<!-- END: .main-content -->

					<!-- BEGIN .main-footer -->
					<footer class="main-footer">

					</footer>
					<!-- END: .main-footer -->

				</div>
				<!-- END: .app-main -->

			</div>
			<!-- END: .app-container -->

		</div>
		<!-- END: .app-wrap -->


		<!--
			**********************
			**********************
			Common JS files
			**********************
			**********************
		-->

		<!-- jQuery JS. -->
		<script src="js/jquery.js"></script>

		<!-- Info: jQuery UI is only required for datepicker or any jQueryUI related plugins -->
		<script src="js/jquery-ui.min.js"></script>

		<!-- Tether Js, then other JS. -->
		<script src="js/popper.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="vendor/unifyMenu/unifyMenu.js"></script>
		<script src="vendor/onoffcanvas/onoffcanvas.js"></script>
		<script src="js/moment.js"></script>

		<!-- News Ticker JS -->
		<script src="vendor/newsticker/newsTicker.min.js"></script>
		<script src="vendor/newsticker/custom-newsTicker.js"></script>

		<!-- Slimscroll JS -->
		<script src="vendor/slimscroll/slimscroll.min.js"></script>
		<script src="vendor/slimscroll/custom-scrollbar.js"></script>

		<!-- Daterange JS -->
		<script src="vendor/daterange/daterange.js"></script>
		<script src="vendor/daterange/custom-daterange.js"></script>


		<!--
			**********************
			**********************
			Optional JS files - Plugns
			**********************
			**********************
		-->

		<!-- Morris Graphs JS -->
		<script src="vendor/morris/raphael-min.js"></script>
		<script src="vendor/morris/morris.min.js"></script>
		<script src="vendor/morris/custom/areaChart.js"></script>
		<script src="vendor/morris/custom/line-chart.js"></script>
		<script src="vendor/morris/custom/multibar.js"></script>

		<!-- Circliful JS -->
		<script src="vendor/circliful/circliful.min.js"></script>
		<script src="vendor/circliful/circliful.custom.js"></script>

		<!-- Peity JS -->
		<script src="vendor/peity/peity.min.js"></script>
		<script src="vendor/peity/custom-peity.js"></script>

		<!-- Tag Manager JS -->
		<script src="vendor/tags/tagmanager.js"></script>
		<script src="vendor/tags/tagmanager-custom.js"></script>

		<!-- Common JS -->
		<script src="js/common.js"></script>

		<script>
			//Datepicker
			$(function() {
				$("#datepicker").datepicker();
			});
		</script>
		
	</body>
</html>